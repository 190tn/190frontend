export const environment = {
  production: true,
  socketBaseUrl: 'wss://www.stopcovid-tn.info/covid-190/api/v1/socket',
  apiBaseUrl: '/covid-190/api/v1',
  nodeUrl: ''
};
