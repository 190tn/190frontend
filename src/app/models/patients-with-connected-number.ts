import {PatientModel} from './patient.model';

export interface PatientsWithConnectedNumber {
  connectedPatientsNumber: number;
  patients: PatientModel[];
}
