export class FormSymptomsModel {
  id: number;
  text: string;
  score: number;
  responseDTOS: SuggestionsModule[];
  advice: string;
}

export class SuggestionsModule {
  id: number;
  text: string;
  score: number;
}

export class ResponseModel {
  questionId: number;
  responseIds: number[];
  score: number;
}

export class SubmitResponseModel {
  id: number;
  fullName: string;
  phoneNumber: string;
  regionId: number;
  requestScore: number;
  forms: ResponseModel[];
}
