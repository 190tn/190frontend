export class TeamMembersModel {
    developers: MemberModel[];
    pharmacists: MemberModel[];
    doctors: MemberModel[];
    other: MemberModel[];
}

export class MemberModel {
    fullName: string;
    job: string;
    linkedinUrl: string;
    imgSrc: string;
}
