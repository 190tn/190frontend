import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SignInRequestModel} from '../models/sign-in-request.model';
import {Observable, of} from 'rxjs';
import {environment} from '../../environments/environment';
import {catchError, map} from 'rxjs/operators';
import {LocalStorageUtils} from '../utils/local-storage-utils';
import {REFRECH_TOKEN_KEY} from '../utils/local-storage-constants';
import {AuthenticationProvider} from './authentication-provider';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly headers = new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
    'Authorization': 'Basic ' + btoa('stop-corona-client:stop-corona-secret')});

  constructor(private http: HttpClient,
              private authenticationProvider: AuthenticationProvider) { }

  login(model: SignInRequestModel): Observable<any> {
    return this.http.post(environment.apiBaseUrl + '/oauth/token', 'grant_type=password&username=' + model.userName + '&password=' + model.password, {
      headers: this.headers
    }).pipe(
      map(data => {
        LocalStorageUtils.saveTokenFieldsInLocalStorage(data);
        this.authenticationProvider.setConnected(true);
       })
    );
  }

  refreshToken(): Observable<any> {
    return this.http.post(environment.apiBaseUrl + '/oauth/token', 'grant_type=refresh_token&refresh_token=' +
      LocalStorageUtils.getItem(REFRECH_TOKEN_KEY), {headers: this.headers})
      .pipe(
        map(data => {
          LocalStorageUtils.saveTokenFieldsInLocalStorage(data);
          this.authenticationProvider.setConnected(true);
          return of(true);
        }),
        catchError( () => of(false)));
  }

  logout() {
    return this.http.get(environment.apiBaseUrl + '/doctors/logout');
  }
}
