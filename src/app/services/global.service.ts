import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {CityModel} from '../models/city.model';
import {HttpClient} from '@angular/common/http';
import {TeamMembersModel} from '../models/team.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor(private http: HttpClient) { }

  getCities(): Observable<CityModel[]>  {
    return this.http.get<CityModel[]> (environment.apiBaseUrl+"/regions/all");
  }
  getTeamMembers(): Observable<TeamMembersModel>  {
    return this.http.get<TeamMembersModel> ('../../assets/data/team.json');
  }
}
