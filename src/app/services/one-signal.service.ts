import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

// TODO delete console.log after tests.
@Injectable({
  providedIn: 'root'
})
export class OneSignalService {

  OneSignal: any = [];

  constructor(private readonly httpClient: HttpClient) {
  }

  initOneSignal() {
    this.OneSignal = window['OneSignal'] || [];
    this.OneSignal.push(['init', {
      appId: '283848e5-c3b5-4231-833e-f63679b19b0f',
      allowLocalhostAsSecureOrigin: true,
      autoRegister: true,
      notifyButton: {
        enable: false,
      },
    }]);
    console.log('Initialize OneSignal');
    this.checkIfUserIsSubscribed();
  }

  checkIfUserIsSubscribed() {
    this.OneSignal.push(() => {
      this.OneSignal.isPushNotificationsEnabled((isEnabled) => {
        if (isEnabled) {
          console.log('Push notifications are enabled!');
          this.getUserId();
        } else {
          console.log('Push notifications are not enabled yet.');
          this.subscribe();
        }
      }, (error) => {
        console.log(error);
      });
    });
  }

  subscribe() {
    this.OneSignal.push(() => {
      console.log('Register For Push');
      this.OneSignal.push(['registerForPushNotifications']);
      this.OneSignal.on('subscriptionChange', (isSubscribed) => {
        console.log('The user subscription state is now:', isSubscribed);
        this.listenForNotification();
        this.getUserId();
      });
    });
  }

  listenForNotification() {
    console.log('Initalize Listener');
    this.OneSignal.on('notificationDisplay', (event) => {
      console.log('this.OneSignal notification displayed:', event);
      this.getUserId();
    });
  }

  getUserId() {
    this.OneSignal.getUserId().then((userId) => {
      console.log('User ID is', userId);
      localStorage.setItem('oneSignalId', userId);
    });
  }

}
