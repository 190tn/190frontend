import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {FormSymptomsModel} from '../models/form-symptoms.model';
import { SubmitResponseModel } from './../models/form-symptoms.model';
import {environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SymptomsFormService {

  constructor(private http: HttpClient) { }

  getQuestions(): Observable<FormSymptomsModel[]>  {
    return this.http.get<FormSymptomsModel[]> (environment.apiBaseUrl + '/questions/all');
  }

  submitResponse(response: SubmitResponseModel): Observable<any>  {
    return this.http.post (environment.apiBaseUrl + '/questions/save',  response);
  }
}
