import {Observable} from 'rxjs';
import {PatientModel} from '../models/patient.model';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {PatientsWithConnectedNumber} from '../models/patients-with-connected-number';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient) { }

  getInProgressPatients(): Observable<PatientsWithConnectedNumber> {
    return this.http.get<PatientsWithConnectedNumber>(environment.apiBaseUrl + '/patients/in-progress');
  }

  getFirstConnectedPatientForCall(): Observable<PatientModel> {
    return this.http.get<PatientModel>(environment.apiBaseUrl + '/patients/first-to-call');
  }


  directCall(patientId?): Observable<any> {
    if(patientId)
      return this.http.post(environment.apiBaseUrl + `/patients/${patientId}/DIRECT_CALL`, null);
    else
      return this.http.get(environment.apiBaseUrl+"/patients/direct-call")
  }
  endCall(patientId: any, status: any, comment:any) {
    return this.http.post(environment.apiBaseUrl + `/patients/${patientId}/${status}`, {comment: comment});
  }
}
