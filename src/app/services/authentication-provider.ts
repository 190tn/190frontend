import {Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, flatMap, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationProvider {

  private connected = new Subject<boolean>();

  constructor(private http: HttpClient) { }

  isConnected(): Observable<boolean> {
    return this.http.get<boolean> (environment.apiBaseUrl + '/doctors/is-connected');
  }

  getConnected(): Observable<boolean> {
   return this.connected.asObservable();
  }

  setConnected(value: boolean) {
    this.connected.next(value);
  }

}
