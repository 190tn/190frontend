import { ResponseModel, SubmitResponseModel } from './../../models/form-symptoms.model';
import {Component, OnInit, Input} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SymptomsFormService } from 'src/app/services/symptoms-form.service';
import {CityModel} from '../../models/city.model';
import {GlobalService} from '../../services/global.service';
import {environment} from '../../../environments/environment';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-contact-doctor',
  templateUrl: './contact-doctor.component.html',
  styleUrls: ['./contact-doctor.component.scss']
})
export class ContactDoctorComponent implements OnInit {

  public registerForm: FormGroup;
  submitted = false;
  serverError = '';
  cities: CityModel[];
  @Input() response: Array<ResponseModel>;
  showIframeCall = false;
  showContactForm = true;
  uuid;
  delegations=[]


  constructor(private symptomsFormService: SymptomsFormService, private globalService: GlobalService, public formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {
    
  }

  ngOnInit() {
    this.getCities();
    this.registerForm = this.formBuilder.group({
      fullName: ['', [Validators.required, Validators.minLength(6)]],
      phoneNumber: ['', [Validators.required]],
      regionId: ['', Validators.required],
      delegationId: [''],
      validateForm: [false, Validators.required ],
      priorityForm: [false, Validators.required ]
      }
    );

    this.registerForm.controls.regionId.valueChanges.subscribe(evt => {
      const selected = this.cities.filter(c => c.id == evt)
      if(selected && selected.length){
        this.delegations = selected[0].delegations;
      }
    })

    this.route.queryParams.subscribe(qp => {
      this.uuid=qp.patient;
      if(this.uuid)
        this.showCall(this.uuid)
    })
  }

  private getCities(): void {
    this.globalService.getCities().subscribe(data => {
      this.cities  = data;
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

 onSubmit() {
   this.submitted = true;
   // stop here if form is invalid
   if (this.registerForm.invalid) {
       return;
   }
   const register: SubmitResponseModel = this.registerForm.value;
   register.forms = this.response;
   register.requestScore = this.response.map(x => x.score).reduce((a, b) => a + b, 0);
   this.symptomsFormService.submitResponse(register).subscribe(data => {
      this.router.navigate(["/formulaire-symptômes"],{queryParams: {patient: data.uuid}})
   }, error =>  {
      this.serverError = 'نعتذر منك ! هناك خلل تقني، نحن نعمل على إصلاحه';
   });
 }

  disabledAgreement: boolean = true;
  changeCheck(event){
    console.log('1' + this.disabledAgreement);
  }

  showCall(uuid){
    this.showIframeCall = true;
    this.showContactForm = false;
    (<HTMLImageElement>document.getElementById('call_doctor_iframe')).src = environment.nodeUrl + '/session/create/' + uuid;
  }

}
