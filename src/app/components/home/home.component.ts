import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {NgbModalConfig, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('content') content;
  constructor(config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
  }

  ngOnInit() {}

  ngAfterViewInit() {
    if (localStorage.getItem('acceptTerms') !== 'true') {
      // setTimeout important to avoid an Angular bug with ngAfterViewInit lifehook
      setTimeout(() => this.openModal());
    }
  }

  openModal() {
    this.modalService.open(this.content, {centered: true});
  }

  acceptTerms() {
    localStorage.setItem('acceptTerms', 'true');
  }
}
