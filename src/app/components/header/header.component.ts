import { Component, OnInit } from '@angular/core';
import {AuthenticationProvider} from '../../services/authentication-provider';
import {LocalStorageUtils} from '../../utils/local-storage-utils';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isConnected = false;

  constructor(private readonly authenticationProvider: AuthenticationProvider,
              private readonly authenticationService: AuthenticationService) {
    this.authenticationProvider.getConnected().subscribe(
      res => this.isConnected = res,
      () => this.isConnected = false
    );
  }

  ngOnInit() {
    this.authenticationProvider.isConnected().subscribe(
      res => this.isConnected = res
    );
  }

  signOut(): void {
     this.authenticationService.logout().subscribe(
       () => {
         LocalStorageUtils.removeTokenFieldsInLocalStorage();
         this.isConnected = false;
       }
     );
  }

}
