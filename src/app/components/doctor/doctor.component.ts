import { Component, OnDestroy, OnInit, ViewChild, ElementRef, HostListener, Renderer2 } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { PatientService } from "../../services/patient.service";
import { PatientModel } from "../../models/patient.model";
import { switchMap } from 'rxjs/operators';
import { OneSignalService } from '../../services/one-signal.service';
import { environment } from 'src/environments/environment';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

declare var $: any

@Component({
  selector: "app-doctor",
  templateUrl: "./doctor.component.html",
  styleUrls: ["./doctor.component.scss"]
})
export class DoctorComponent implements OnInit, OnDestroy {

  inProgressPatients: PatientModel[];
  noPatientInCall = false;
  showIframeCall = false;
  patient;
  callAvailable = false;
  ringTimeout;
  directCallDone = false;
  doctorDecisionGroup: FormGroup;

  @ViewChild("ringSound") ringSound: ElementRef;
  ringSoundElement: HTMLAudioElement;

  constructor(private readonly patientService: PatientService,
    private readonly oneSignalService: OneSignalService, private fb: FormBuilder, private rendrer: Renderer2) { }

  ngOnInit() {
    this.initWs()
    this.oneSignalService.initOneSignal();
    this.loadPatients()
    this.ringSoundElement = this.ringSound.nativeElement;
    this.doctorDecisionGroup = this.fb.group({
      status: [null, [Validators.required]],
      patient: [null, [Validators.required]], 
      comment: [null, [Validators.required]]
    })
    this.doctorDecisionGroup.controls.status.valueChanges.subscribe(val => $('#directCallDetail').modal('handleUpdate'))
    this.checkDirectCall();
  }

  initWs = ()=>{
    var conn = new WebSocket(environment.socketBaseUrl + "/patient")
    conn.onmessage = this.socketMessage
    conn.onerror = () =>{
      conn.close()
    }
    conn.onclose = () => {
      setTimeout(()=>{
        this.initWs()
      }, 1000)
    }
  }

  @HostListener('window:message', ['$event']) onPostMessage(event) {
    if (event.data == "end") {
      this.patient = null;
      this.showIframeCall = false;
    }
  }

  goToCall() {
    this.noPatientInCall = false;
    this.patientService.getFirstConnectedPatientForCall().subscribe(patient => {
      this.patient = patient
      this.showInfo()
      if (!!patient) {
        this.showIframeCall = true;
        (<HTMLImageElement>document.getElementById('call_patient_iframe')).src = environment.nodeUrl + '/session/connect/' + patient.sessionId;
      } else {
        this.showIframeCall = false;
        this.noPatientInCall = true;
      }
    });
  }

  ngOnDestroy(): void {
    //this.patientSubscription.unsubscribe();
  }


  showInfo() {
    $("#patientDetail").modal("show")
  }

  socketMessage = (message) => {
    const data = JSON.parse(message.data);
    switch (data.type) {
      case "UPDATE":
        this.updatePatients(data.data);
    }
  }

  loadPatients() {
    this.patientService.getInProgressPatients().subscribe(result => {
      this.updatePatients(result)
    }
    );
  }

  updatePatients(patients) {
    this.inProgressPatients = patients.patients;
    let patientFound = false;
    patients.patients.forEach(patient => {
      if(patient.connected && !patient.callInProgress){
        patientFound = true;
        return true;
      }
    })
    console.log("found:"+patientFound)
    if (patientFound) {
      this.ringTimeout = setTimeout(() => {
        this.ringSoundElement.currentTime = 0;
        this.ringSoundElement.muted = false;
        console.log("1:"+this.ringSoundElement.muted)
      }, 2000)
      
      this.callAvailable = true
    } else {
      if(this.ringTimeout){
        clearTimeout(this.ringTimeout)
      }
      this.ringSoundElement.muted=true
      console.log("2:"+this.ringSoundElement.muted)
      this.callAvailable = false;
    }
  }

  showPatientDetail(patient){
    this.patient = patient;
    this.showInfo();
  }

  directCall(patientId, phoneNumber){
    this.patientService.directCall(patientId).subscribe(res => {
      $("#patientDetail").modal("hide")
      $("#directCallDetail").modal("hide")
      this.patient = null;

      var phoneElement: HTMLAnchorElement = this.rendrer.createElement("a")
      phoneElement.href = "tel:"+phoneNumber;
      phoneElement.click()
      this.checkDirectCall()
    })
  }

  checkDirectCall(){
    this.patientService.directCall().subscribe(patient => {
      if(patient){
        $("#directCallDetail").modal({
          backdrop:'static',
          keyboard: false
        })
        this.doctorDecisionGroup.controls.patient.setValue(patient);
      }else{
        $("#directCallDetail").modal("hide")
      }
    })
  }

  submitDecision(){
    this.patientService.endCall(
      this.doctorDecisionGroup.controls.patient.value.id,
      this.doctorDecisionGroup.controls.status.value,
      this.doctorDecisionGroup.controls.comment.value).subscribe(res => {
      this.doctorDecisionGroup.reset()
      this.checkDirectCall()
    })
  }
}
