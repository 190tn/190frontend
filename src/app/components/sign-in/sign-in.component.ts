import { Component, OnInit } from '@angular/core';
import {SignInRequestModel} from '../../models/sign-in-request.model';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  signInRequestModel: SignInRequestModel = {
    userName: '',
    password: ''
  };
  displaySignInFailedMessage = false;

  constructor(private readonly authenticationService: AuthenticationService,
              private readonly router: Router) { }

  ngOnInit() {
  }

  signIn() {
    this.displaySignInFailedMessage = false;
    this.authenticationService.login(this.signInRequestModel).subscribe(response => {
      this.router.navigate(['médecin']);
    }, (error => this.displaySignInFailedMessage = true));
  }



}
