import { Component, OnInit } from '@angular/core';
import { SymptomsFormService } from '../../services/symptoms-form.service';
import { FormSymptomsModel, ResponseModel } from '../../models/form-symptoms.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-symptoms-form',
  templateUrl: './symptoms-form.component.html',
  styleUrls: ['./symptoms-form.component.scss']
})
export class SymptomsFormComponent implements OnInit {
  questions: FormSymptomsModel[];
  formScore = 0;
  messageCoronaTitle: string;
  messageCorona: string;
  isCoronaCase: boolean;
  callDoctor: boolean;
  currentQuestionIndex = 0;
  showedQuestions: FormSymptomsModel[] = [];
  questionsNum = 0;
  errorMessage: string;
  forms: Array<ResponseModel> = [];

  constructor(private symptomsFormService: SymptomsFormService, private route: ActivatedRoute) {

    this.route.queryParams.subscribe(qp => {
      if (qp.patient)
        this.updateForm(4)
    })
  }

  ngOnInit() {
    this.getQuestions();
  }

  private getQuestions(): void {
    this.symptomsFormService.getQuestions().subscribe(
      data => {
        this.questions = data;
        this.showedQuestions.push(this.questions[0]);
        this.questionsNum = this.questions.length;
      },
      error => {
        this.errorMessage = 'نعتذر منك ! هناك خلل تقني، نحن نعمل على إصلاحه';
      });
  }

  private isNext(): boolean {
    return !(this.currentQuestionIndex === (this.questionsNum - 1));
  }

  private next(): void {
    if (this.isNext()) {
      this.showedQuestions.pop();
      this.currentQuestionIndex = this.currentQuestionIndex + 1;
      this.showedQuestions.push(this.questions[this.currentQuestionIndex]);
    } else {
      this.getFormResults();
    }
  }

  private calculateScore(questionId, suggestionId, suggestionScore, questionScore): void {
    this.formScore += (suggestionScore * questionScore);
    const index = this.forms.findIndex(x => x.questionId === questionId);
    if (index === -1) {
      const form = new ResponseModel();
      form.questionId = questionId;
      form.responseIds = Array.of(suggestionId);
      form.score = suggestionScore * questionScore;
      this.forms.push(form);
    } else {
      this.forms[index].responseIds = Array.of(suggestionId);
      this.forms[index].score = suggestionScore * questionScore;
    }
  }

  private getTotalScore(): number {
    return this.forms.map(x => x.score).reduce((a, b) => a + b, 0);
  }

  private getFormResults(): void {
    this.formScore = this.getTotalScore();
    this.updateForm(this.formScore)
  }

  updateForm(formScore) {
    if (formScore >= 3)
      this.callDoctor = true;
    if (formScore >= 4) {
      this.messageCoronaTitle = 'إجاباتك تتضمّن بعض الأعراض المحتملة لفيروس الكورونا';
      this.messageCorona = 'يلزمك تحط روحك في الحجر الصحي و ما يلزمش تمشي ل المستشفى باش ما تعديش ناس أخرين.\n دخّل معلوماتك تو يجاوبك طبيب ينصحك و يقلك شنوا تعمل\n' +
        'كان متحصّلتش علينا كلّم ال 190';
      this.isCoronaCase = true;
    } else {
      this.messageCoronaTitle = 'إجاباتك ما تدلّش إلّي عندك أعراض مستعجلة و خطيرة متاع إصابة بالكورونا';
      this.messageCorona = 'تبع حالتك الصحيّة و عاود كلمنا إذا ظهرت عندك أعراض الكورونا (سخانة، كحّة شايحة، ضيق في التنّفس) إذا مازلت تحب تطمّن تنجّم تكلّم طبيب عن طريق الموقع باش تزيد تتطمّن';
    }
  }
}
