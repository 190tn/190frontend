import { Component, OnInit } from '@angular/core';
import {GlobalService} from '../../services/global.service';
import {TeamMembersModel} from '../../models/team.model';

@Component({
  selector: 'app-team-members',
  templateUrl: './team-members.component.html',
  styleUrls: ['./team-members.component.scss']
})
export class TeamMembersComponent implements OnInit {
  team: TeamMembersModel;

  constructor(private globalService: GlobalService) { }

  ngOnInit() {
    this.getMembers();
  }

  private getMembers(): void {
    this.globalService.getTeamMembers().subscribe(data => {
      this.team  = data;
    });
  }

}
