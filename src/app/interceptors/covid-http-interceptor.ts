import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {LocalStorageUtils} from '../utils/local-storage-utils';
import {ACCESS_TOKEN_KEY, EXPIRES_IN_KEY} from '../utils/local-storage-constants';
import {flatMap} from 'rxjs/operators';
import {AuthenticationService} from '../services/authentication.service';
import {Router} from '@angular/router';

@Injectable()
export class CovidHttpInterceptor implements HttpInterceptor {

   private PUBLIC_URIS = ['/oauth/token', '/questions/all', '/regions/all', '/doctors/is-connected', '/questions/save'];

   constructor(private authenticationService: AuthenticationService,
               private router: Router){}

  intercept(request: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {
    let isPublicUri = false;
    this.PUBLIC_URIS.forEach(url => {
      if (request.url.endsWith(url)) {
        isPublicUri = true;
      }
    });
    if (!isPublicUri) {
      if (+ LocalStorageUtils.getItem(EXPIRES_IN_KEY) - new Date().getTime() < 120 * 1000) {
         return this.refreshToken(request, next);
      }
      const clonedRequest = request.clone(this.setHeaders());
      return next.handle(clonedRequest);
    }
    return next.handle(request);
  }

  private refreshToken(request: HttpRequest<any>,
                         next: HttpHandler): Observable<HttpEvent<any>> {
    const clonedRequest = request.clone(this.setHeaders());
    return this.authenticationService.refreshToken().pipe(
       flatMap(yes => {
          if (!yes) {
            this.router.navigate(['/se-connecter']);
            return of();
          }
          return next.handle(clonedRequest);
       })
     );
  }

  private setHeaders() {
     return {
       setHeaders: {
         'Authorization': 'Bearer ' + LocalStorageUtils.getItem(ACCESS_TOKEN_KEY)
       }
     };
  }

}
