import {ACCESS_TOKEN_KEY, EXPIRES_IN_KEY, REFRECH_TOKEN_KEY} from './local-storage-constants';


export class LocalStorageUtils {

  public static setItem(key: string, value: any) {
    if (key && value) {
      localStorage.setItem(key, value);
    }
  }

  public static getItem(key: string) {
    return key ? localStorage.getItem(key) : null;
  }

  public static saveTokenFieldsInLocalStorage(token: any) {
    LocalStorageUtils.setItem(ACCESS_TOKEN_KEY, token.access_token);
    LocalStorageUtils.setItem(REFRECH_TOKEN_KEY, token.refresh_token);
    LocalStorageUtils.setItem(EXPIRES_IN_KEY, new Date().getTime() + (1000 * token.expires_in));
  }

  public static removeTokenFieldsInLocalStorage() {
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    localStorage.removeItem(REFRECH_TOKEN_KEY);
    localStorage.removeItem(EXPIRES_IN_KEY);
  }
}
